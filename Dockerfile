FROM node:8-alpine

#RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

EXPOSE 80

COPY package.json package-lock.json /usr/src/app/
RUN npm install --only=production
COPY dist /usr/src/app/

ENTRYPOINT ["node", "index.js"]
