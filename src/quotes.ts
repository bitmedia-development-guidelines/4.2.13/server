//from: https://www.javacodegeeks.com/2012/11/20-kick-ass-programming-quotes.html
//from: http://www.devtopics.com/101-great-computer-programming-quotes/
//from: https://fortrabbit.github.io/quotes/
export const QUOTES = [
    {
        text: 'First, solve the problem. Then, write the code.',
        who: 'John Johnson'
    },
    {
        text: 'Optimism is an occupational hazard of programming; feedback is the treatment.',
        who: 'Kent Beck'
    },
    {
        text: 'There is no programming language–no matter how structured–that will prevent programmers from making bad programs.',
        who: 'Larry Flon'
    },
    {
        text: 'The best engineers I know are artists at heart. The best designers I know are secretly technicians as well.',
        who: 'Andrei Herasimchuk'
    },
    {
        text: 'Good code is its own best documentation.',
        who: 'Steve McConnell'
    },

    {
        text: 'If you can\'t write it down in English, you can\'t code it.',
        who: 'Peter Halpern'
    },
    {
        text: 'The goal of Computer Science is to build something that will last at least until we´ve finished building it.',
        who: 'Unknown'
    },
    {
        text: 'Adding manpower to a late software project makes it later!',
        who: 'Fred Brooks'
    },
    {
        text: 'You can never underestimate the stupidity of the general public.',
        who: 'Scott Adams'
    },
    {
        text: 'The greatest performance improvement of all is when a system goes from not-working to working.',
        who: 'John Ousterhout'
    },
    {
        text: 'We cannot learn without pain.',
        who: 'Aristotle'
    },
    {
        text: 'Pasting code from the Internet into production code is like chewing gum found in the street.',
        who: 'Unknown'
    },
    {
        text: 'You start coding. I’ll go find out what they want.',
        who: 'Unknown'
    },
    {
        text: 'There’s no test like production.',
        who: 'Unknown'
    },
    {
        text: 'Experience is the name everyone gives to their mistakes.',
        who: 'Oscar Wilde'
    },
    {
        text: 'I don’t care if it works on your machine!  We are not shipping your machine!',
        who: 'Vidiu Platon'
    },
    {
        text: 'Beta is Latin for still doesn’t work.',
        who: 'Unknown'
    },
    {
        text: 'A computer program does what you tell it to do, not what you want it to do.',
        who: 'Unknown'
    },
    {
        text: 'Always code as if the guy who ends up maintaining your code will be a violent psychopath who knows where you live.',
        who: 'Martin Golding'
    },
    {
        text: 'Good programmers use their brains, but good guidelines save us having to think out every case.',
        who: 'Francis Glassborow'
    },
    {
        text: 'Any code of your own that you haven’t looked at for six or more months might as well have been written by someone else.',
        who: 'Eagleson’s Law'
    },
    {
        text: 'For a long time it puzzled me how something so expensive, so leading edge, could be so useless.  And then it occurred to me that a computer is a stupid machine with the ability to do incredibly smart things, while computer programmers are smart people with the ability to do incredibly stupid things.  They are, in short, a perfect match.',
        who: 'Bill Bryson'
    },
    {
        text: 'Before software can be reusable it first has to be usable.',
        who: 'Michael Sinz'
    },
    {
        text: 'No matter how slick the demo is in rehearsal, when you do it in front of a live audience, the probability of a flawless presentation is inversely proportional to the number of people watching, raised to the power of the amount of money involved.',
        who: 'Mark Gibbs'
    },
    {
        text: 'The trouble with programmers is that you can never tell what a programmer is doing until it’s too late.',
        who: 'Seymour Cray'
    },
    {
        text: 'Never trust a computer you can’t throw out a window.',
        who: 'Steve Wozniak'
    },
    {
        text: 'The city’s central computer told you?  R2D2, you know better than to trust a strange computer!',
        who: 'C3PO'
    },
    {
        text: 'Any fool can write code that a computer can understand. Good programmers write code that humans can understand.',
        who: 'Martin Fowler (author and speaker on software development)'
    },
    {
        text: 'Most good programmers do programming not because they expect to get paid or get adulation by the public, but because it is fun to program.',
        who: 'Linus Torvalds (Finnish American, software engineer and hacker, principal force behind the development of the Linux kernel)'
    },
    {
        text: 'Nine people can’t make a baby in a month.',
        who: 'Fred Brooks (American computer scientist, winner of the 1999 Turing Award)'
    },
    {
        text: 'When debugging, novices insert corrective code; experts remove defective code.',
        who: 'Richard Pattis'
    }
];