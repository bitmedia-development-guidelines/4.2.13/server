import * as connect from 'connect';
import * as url from 'url';
import * as  bodyParser from 'body-parser';
import { GraphQLOptions } from 'apollo-server-core';
import { graphqlConnect, graphiqlConnect } from 'apollo-server-express';
import * as http from 'http';
import { makeExecutableSchema } from 'graphql-tools';
import * as dotenv from 'dotenv';
import * as cors from 'cors';

import { QUOTES } from './quotes';

dotenv.config();

const typeDefs = `
   type Quote {
       text: String!
       who: String!
   }
    type Query {
        randomQuote: Quote
        quotes: [Quote]
    }
`;

const PUBLIC_URL = process.env.PUBLIC_URL;
if (!PUBLIC_URL) {
    throw new Error(`PUBLIC_URL env-var must be set`);
}

const PORT = process.env.PORT || 80;
const CORS_WHITELIST = (process.env.CORS_WHITELIST || '').split(' ').filter(x => !!x);
const CORS_ANY = !!CORS_WHITELIST.find(x => x === '*');
const CDN_URL = process.env.CDN_URL;
const UI_PATTERN = process.env.UI_PATTERN;

const resolvers = {
    Query: {
        randomQuote: () => {
            const index = Math.floor(Math.random() * QUOTES.length);
            return QUOTES[index];
        },
        quotes: () => {
            return QUOTES;
        }
    }
}
async function main() {
    const parsedPublicUrl = url.parse(PUBLIC_URL);
    const schema = makeExecutableSchema({
        typeDefs,
        resolvers
    });
    const app = connect();
    app.use(cors({
        origin: (origin, callback) => {
            if (CORS_ANY || !origin) {
                callback(null, true);
                return;
            }
            if (CORS_WHITELIST.indexOf(origin) !== -1) {
                callback(null, true);
                return;
            }
            console.log(`Access from ${origin} was denied`);
            callback(new Error(`Access from ${origin} not allowed by CORS`));
        }
    }));
    app.use('/graphql', bodyParser.json());
    const options: GraphQLOptions = {
        schema: <any>schema
    };
    app.use('/graphql', graphqlConnect(options));
    const graphQLEndpoint = url.resolve(PUBLIC_URL, 'graphql');
    app.use('/graphiql', graphiqlConnect({
        endpointURL: graphQLEndpoint
    }));

    if (CDN_URL) {
        app.use((req: http.IncomingMessage, res: http.ServerResponse) => {
            const r = url.parse(req.url);
            const resolvedPattern = UI_PATTERN
                .replace('${PUBLIC_URL}', PUBLIC_URL)
                .replace('${PUBLIC_URL_ENCODED}', encodeURIComponent(PUBLIC_URL))
                .replace('${NAVIGATION_TARGET}', r.path.substr(1));
            const u = url.resolve(CDN_URL, resolvedPattern);
            console.log(`Redirecting to ${u}`);
            res.writeHead(301, { Location: u });
            res.end();
        });
    }
    const mountAt = connect();
    console.dir(parsedPublicUrl);
    mountAt.use(parsedPublicUrl.path, app);
    const server = http.createServer(mountAt);
    server.listen(PORT);
    console.log(`Server started on port ${PORT}. Please access it with ${PUBLIC_URL}`);
    console.log(`GraphQL endPoint: ${graphQLEndpoint}`);
}

void main();